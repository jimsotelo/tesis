Tesis
======================

Creado por José Manuel Rodríguez Sotelo.
Última actualización: Julio 2013

El objetivo de este trabajo es doble. Por un lado, como trabajo de investigación presenta un nuevo algoritmo para seleccionar seleccionar la función de costo en el método de factorización de matrices no negativas. El método de factorización de matrices no negativas fue popularizado por Lee y Seung en la comunidad de Aprendizaje de Máquina por sus propiedades para aprender estructuras basadas en partes.

# Por hacer:

 * Incluir descarga de datos base y procesamiento inicial en Makefile
 * Subir los datos a un servidor (posiblemente Dropbox)
 * Trasladar los archivos de resultados de Matlab a R
 * Continuar escribiendo los capítulos que faltan
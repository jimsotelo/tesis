####
# Ejemplos de la base.
#
####
source('R/grafCara.R')
C <- c(500, 1000, 1500, 2000)
X <- as.matrix(read.csv('R/Data/caras.csv', row.names = 1))
lapply(C, grafCara, D = t(X), nrow = 19, name = "cara")
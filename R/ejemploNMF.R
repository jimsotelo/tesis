#####
# Aproximar algunas de las caras de la base con el metodo de NMF.
# k <- Numero de componentes que se van a usar en la reconstruccion.
#####
source('R/repmat.R')
source('R/clipImage.R')
source('R/nmf.R')
source('R/grafCara.R')

X <- as.matrix(read.csv('R/Data/caras.csv', row.names = 1))
m <- dim(X)[1]
n <- dim(X)[2]

X <- X-repmat(t(colMeans(X)),m,1)
X <- (X * repmat( 0.25/t(apply(X,2,sd)),m,1)) + .25
X <- clipImage(X,0,1)

k <- 40
beta <- 0

set.seed(1)

W = matrix(runif(m*k), nrow = m)
W = W / ( matrix(rep(1,m), nrow = m) %*% t(colSums(W)))
H = matrix(runif(k*n), nrow = k)
n_iter = 100

flag <- TRUE
j <- 1
while(flag)
{
  Res <- nmf(X, beta, n_iter, W, H)
  W <- Res$W
  H <- Res$H
  flag <- Res$cost[1] - Res$cost[length(Res$cost)] > 10
  j <- j+1
  print(j)
}

lapply(1:40, grafCara, D = t(W), nrow = 19, name = "nmf")
C <- c(500, 1000, 1500, 2000)
lapply(C, grafCara, D = t(W %*% H)-t(X), nrow = 19, name = "est_nmf_beta_0")
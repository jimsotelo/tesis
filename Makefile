# Archivo para compilar tesis.
#Matlab/Fig/beta0.eps: Matlab/graphDiv0.m
#	matlab -nosplash -nodesktop -r "run Matlab/graphDiv0; close all; quit();"

# Comandos antes de compilar.

PREF = $(wildcard Pref/*.aux)
CAP = $(wildcard Cap/*.aux)
FIG = $(wildcard Matlab/Fig/*.pdf)
CAPTEX = $(wildcard Cap/*.tex)

.PHONY: clean fullclean

tesis.pdf: tesis.tex Fig/cara_500.eps Fig/pc_1.eps Fig/nmf_1.eps Fig/div_0_10.eps Fig/dist_10.eps $(CAPTEX)
	latex tesis
	bibtex tesis	
	latex tesis
	latex tesis
	dvipdf tesis
	#open tesis.pdf

Data/Faces/face00001.pgm:
	curl -O http://people.csail.mit.edu/brussell/courses/6.899/nmf/nmf.tar.gz
	tar -zxvf nmf.tar.gz nmf/MyData/faces19x19
	mv nmf/MyData/faces19x19/* Data/Faces
	rm -rf nmf
	rm nmf.tar.gz

R/Data/caras.csv: Data/Faces/face00001.pgm R/leerCaras.R
	R --slave -f R/leerCaras.R

Fig/cara_500.eps: R/Data/caras.csv R/ejemploCaras.R
	R --slave -f R/ejemploCaras.R
	bash Fig/crop_all.sh

Fig/pc_1.eps est_pca_500.eps: R/Data/caras.csv R/ejemploComponentes.R
	R --slave -f R/ejemploComponentes.R
	bash Fig/crop_all.sh

Fig/nmf_1.eps est_nmf_500.eps: R/Data/caras.csv R/ejemploNMF.R
	R --slave -f R/ejemploNMF.R
	bash Fig/crop_all.sh

Fig/div_0_10.eps: R/ejemploDiv.R
	R --slave -f R/ejemploDiv.R

Fig/dist_10.eps: R/ejemploDist.R
	R --slave -f R/ejemploDist.R

clean:
	rm tesis.aux
	rm tesis.dvi
	rm tesis.log
	rm tesis.out
	rm tesis.toc
	rm tesis.bbl
	rm tesis.blg
	rm tesis.lof
	rm tesis.pdf
	rm $(PREF)
	rm $(CAP)

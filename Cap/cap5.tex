\chapter{Familia de Distribuciones Tweedie}

En este cap'itulo se presenta la familia de distribuciones Tweedie que se usar'a para construir el modelo estad'istico que servir'a de base para la elecci'on de la funci'on de p'erdidas en el problema de factorizaci'on de matrices no negativas. La familia de distribuciones Tweedie tiene como casos especiales a las distribuciones Normal, Gamma y Poisson y es un caso especial de los modelos de dispersi'on exponencial.

\section{Modelos de Dispersi'on Exponencial}

Los modelos de dispersi'on exponencial (MDE) son una familia de distribuciones con dos par'ametros que consiste en una familia lineal exponencial con un par'ametro de dispersi'on adicional. Esta familia de modelos es importante en estad'istica por su uso en los modelos lineales generalizados y fueron establecidos como un campo de estudio de inter'es por \citet{Jorgensen87} que estudi'o con detalle sus propiedades.

\begin{definicion}[Modelo de Dispersi'on Exponencial]
Sea $Y$ una variable aleatoria cuya funci'on de distribuci'on se puede escribir de la forma:
\begin{align*}
	f(y;\theta,\phi) = a(y,\phi) \exp\left[\frac{1}{\phi}(y\theta-\kappa(\theta))\right]
\end{align*}
Entonces se dice que $Y$ es un modelo de dispersi'on exponencial con par'ametro de dispersi'on $\phi >0$ y par'ametro can'onico $\theta$.
\end{definicion}

Cualquier MDE se puede caracterizar por su funci'on varianza $ V() $, que describe la relaci'on entre la media y la varianza de la distribuci'on cuando se fija el par'ametro de dispersi'on. Si $ Y $ sigue la distribuci'on de un MDE con media $ \mu $, funci'on varianza $ V() $ y par'ametro de dispersi'on $ \phi $, entonces la varianza de $ Y $ se calcula como:
\begin{center}
	$var(Y) = \phi V(\mu)$
\end{center}
en donde $\phi$ es el par'ametro de dispersi'on.

$\kappa (\theta )$ es llamada la funci'on cumulante. Si $\phi = 1$, la media y varianza de la distribuci'on se pueden calcular como $\kappa ^{\prime } (\theta)=\mu $ y $\kappa ^{\prime \prime }(\theta)=V(\mu )$ respectivamente. En general, no se puede obtener una expresi'on anal'itica para $a(y,\phi)$, lo cual resulta en que sea dif'icil de evaluar la funci'on de distribuci'on.

Los modelos de dispersi'on exponencial tienen una funci'on generadora de momentos simple, que se suele usar para evaluarlos. La funci'on generadora de momentos es $M(t) = \int exp(ty) f(y;\theta,\phi) dy$. Sustituyendo la funci'on de densidad, se obtiene que la funci'on generadora de momentos es:
\begin{align*}
	M(t) = exp \left( \frac{\kappa(\theta + t \phi) - \kappa(\theta)}{\phi} \right)
\end{align*}

\section{Distribuciones Tweedie}
\begin{definicion}[Familia de distribuciones Tweedie]
Sea $Y$ un modelo de dispersi'on exponencial con media $\mu$ y par'ametro de dispersion $\phi$ tal que la varianza cumple $V(\mu )=\phi \mu ^{p},\;p\in \Re \backslash (0,1)$. Entonces se dice que $Y$ es una distribuci'on de la familia Tweedie con par'ametro $p$.
\end{definicion}

La familia de distribuciones Tweedie incluye la mayor'ia de las distribuciones comunmente asociadas con los modelos lineales generalizados, incluyendo la distribuci'on normal ($p=0$), Poisson ($p=1$), gamma ($p=2$), entre otras. A pesar de que otros modelos Tweedie han sido menos estudiados y son menos conocidos, esta familia de distribuciones est'a bien definida para todos los valores de $p\in \Re \backslash (0,1)$.

Cuando $p \geq 1$, todas las distribuciones de la familia Tweedie tienen dominio no negativo y medias estrictamente positivas, $\mu > 0$. Por otro lado, las distribuciones de la familia Tweedie en las que $p < 0$ son inusuales ya que cuentan con soporte en toda la recta real pero media estrictamente positiva. El objetivo de este trabajo es usar las distribuciones Tweedie como modelo estad'istico en t'erminos del cual se plantea un problema de inferencia que sea equivalente a la FMN. Debido a esto, las distribuciones con $p < 0$ no son de inter'es puesto que no satisfacen la restricci'on de no negatividad de su dominio y no ser'an consideradas m'as en este trabajo.

Las distribuciones de la familia Tweedie son particularmente favorables para modelar datos continuos y positivos puesto que son los 'unicos MDE cerrados bajo reescalamiento de las variables. Las distribuciones en las que $1<p<2$ son particularmente atractivas cuando existen ceros exactos en los datos debido a que son distribuciones con masa de probabilidad en cero y densidad continua en los n'umeros positivos. \citet{Dunn05} se'nalan algunas de las aplicaciones en las que esta familia de distribuciones es usada e incluye estudios actuariales, de supervivencia, de series de tiempo, de consumo y meteorolog'ia.

La funci'on cumulante y la media (par'ametro can'onico) que satisfacen la propiedad que caracteriza a las distribuciones de la familia Tweedie, $V(\mu )=\mu ^{p}$, son:
\begin{align*}
	\theta = \left\{ 
	\begin{array}{rl}
	\frac{\mu^{1-p}}{1-p}, & \mbox{ si $p\neq 1$} \\ 
	\log\mu, & \mbox{ si $p=1$}%
	\end{array}
	\right., \;\;\;\;\;\; \kappa(\theta) = \left\{ 
	\begin{array}{rl}
	\frac{\mu^{2-p}}{2-p}, & \mbox{ si $p\neq 2$} \\ 
	\log\mu, & \mbox{ si	 $p=2$}%
	\end{array}
	\right.\;.
\end{align*}

\begin{figure}[ht*]
	\centering
	\subfloat[][$ p = 1 $]{
	\includegraphics[height = 45 mm, width = 70 mm]{Fig/dist_10}}
	\subfloat[][$ 1 \leq p < 2 \quad (p=1.5) $]{
	\includegraphics[height = 45 mm, width = 70 mm]{Fig/dist_15}}
	\qquad
	\subfloat[][$ p=2 $]{
	\includegraphics[height = 45 mm, width = 70 mm]{Fig/dist_20}}
	\subfloat[][$ 2 < p \quad (p=4) $]{
	\includegraphics[height = 45 mm, width = 70 mm]{Fig/dist_40}}
	\caption{Funci'on de probabilidad acumulada para distribuciones de la familia Tweedie con distintos valores de $p$ (con $\phi = 1$ y $\mu = 1$).}
\end{figure}

Desafortunadamente, s'olo las distribuciones con $p=0,1,2$ o $3$ cuentan con una funci'on de densidad anal'itica lo cual limita el uso de estas distribuciones en la pr'actica. Por el otro lado, las distribuciones de la familia Tweedie cuentan con una funci'on generadora de momentos simple, lo que se usa como base para evaluar numericamente estas distribuciones.

\citet{Dunn01} exploran alternativas para evaluar las distribuciones usando expansiones de series que aproximan la funcion de densidad. Estos trabajos son complementados por \citet{Dunn07}, que presentan una alternativa usando an'alisis de Fourier. \citet{Dunn14} presenta la implementaci'on en el software estad'istico R de los m'etodos de evaluaci'on que se presentan en este trabajo y que fue utilizado para producir los resultados de este trabajo.

\subsection{Casos Especiales}

Es f'acil ver que las distribuciones Normal, Gamma y Poisson corresponden a densidades de la familia Tweedie con par'ametros 0, 1 y 2 respectivamente. Estas distribuciones son bien conocidas en la literatura. Para una mejor referencia sobre sus propiedades se puede consultar \citet{Krish06}.

\renewcommand{\arraystretch}{1.9}
\begin{table}[thb]
\centering{\begin{tabular}{|c|c|c|c|}
\cline{1-3}
    \textbf{Distribuci'on}&\textbf{p}&\textbf{Funci'on de distribuci'on}\\\hline
    Normal  & $0$ & $f(x; \mu, \sigma) = \frac{1}{\sqrt{2 \pi \sigma}}e^{(\frac{x-\mu}{\sigma})^2} $ \\\hline
    Poisson & $1$ & $f(x; \lambda) = \frac{\lambda^x}{x!}e^{-\lambda}I_{0,1,...}(x)$ \\\hline
    Gamma   & $2$ & $f(x; a, b) = \frac{x^{a-1}e^{-bx}b^{a}}{\Gamma(\alpha)} $ \\\hline
    \end{tabular}}\\   
 \caption{Casos especiales de distribuciones de la familia Tweedie}  
\label{distribuciones_probab_cont}
\end{table}


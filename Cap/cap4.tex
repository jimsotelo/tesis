\chapter{Funciones de divergencia}
\label{cap:diverg}

En el cap'itulo anterior se expuso la factorizaci'on de matrices no negativas y se abord'o la necesidad de escoger una funci'on de costo o p'erdida adecuada. En este cap'itulo se describe la familia de divergencias que es m'as frecuentemente usada para la FMN.

La elecci'on de la funci'on divergencia define la forma de cuantificar las diferencias entre las matrices involucradas. Esto implica asumir ciertas propiedades estad'isticas en los datos. En el cap'itulo \ref{cap:modelo} se exploran las consecuencias de diferentes funciones de costo y se relaciona la elecci'on de cierta funci'on con un modelo estad'istico correspondiente.

Para la funci'on de divergencia se puede considerar cualquier funci'on que cumpla las siguientes caracter'isticas:
\begin{itemize}
	\item $d:\mathbb{X}^2 \rightarrow \mathbb{R}$
	\item $d(u,v) \geq 0 \qquad \forall \ u,\ v \in \mathbb{X}$
	\item $d(u,v) = 0 \iff u = v \qquad \forall \ u,\ v \in \mathbb{X}$
\end{itemize}
Cabe destacar que a diferencia de las distancias, las divergencias no necesitan cumplir simetr'ia ni la desigualdad del tri'angulo.

\section{Casos Especiales}
Aqu'i se presentan algunas de las divergencias m'as comunmente utilizados en la factorizaci'on de matrices no negativas.

\subsection{Divergencia Euclideana}
\label{sec:Euclid}

La divergencia euclidiana es probablemente la m'as usada en la mayor'ia de las aplicaciones. Su origen est'a en la forma tradicional de medir distancias en el plano, y la forma de calcularla es:
\begin{align*}
	d_{\mathrm{EUC}}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad = \quad \frac{1}{2}(v_{fn}-\hat{v}_{fn})^{2}
\end{align*}

\begin{figure}[ht*]
	\centering
	\null\hfill
	\subfloat[][$ v_{fn} = 0.5 $]{
	\includegraphics[]{Fig/div_20_5}}
	\hfill
	\subfloat[][$ v_{fn} = 1$]{
	\includegraphics[]{Fig/div_20_10}}
	\hfill\null
	
	\null\hfill
	\subfloat[][$ v_{fn} = 5$]{
	\includegraphics[]{Fig/div_20_50}}
	\hfill
	\subfloat[][$ v_{fn} = 10$]{
	\includegraphics[]{Fig/div_20_100}}
	\hfill\null
	\caption{Divergencia Euclidiana $ d_{\mathrm{EUC}}\left( v_{fn}\mid \hat{v}_{fn}\right) $ como funci'on de $ \hat{v}_{fn} $.}
	\label{fig:eucdiv}
\end{figure}

\subsection{Divergencia Kullback - Leibler}
Esta divergencia fue presentada por \citet{Kullback51} y se ha usado extensivamente para medir la diferencia entre dos distribuciones de probabilidad; en espec'ifico, sirve para medir la cantidad de informaci'on que se pierde por aproximar una distribuci'on con otra. Algunos ejemplos de usos de esta divergencia incluyen la comparaci'on de cadenas de Markov \citet{Rached04}, la aproximaci'on de funciones de densidad \citet{Georgiou03}, la extracci'on autom'atica de texturas de im'agenes \citet{Do02} y la robustificaci'on de las inferencias en estudios ecol'ogicos \citet{Burnham01}.
	
\begin{align*}
	d_{\mathrm{KL}}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad = \quad v_{fn}\log \frac{v_{fn}}{\hat{v}_{fn}}-v_{fn}+\hat{v}_{fn}
\end{align*}

\begin{figure}[ht*]
	\centering
	\null\hfill
	\subfloat[][$ v_{fn} = 0.5 $]{
	\includegraphics[]{Fig/div_10_5}}
	\hfill
	\subfloat[][$ v_{fn} = 1$]{
	\includegraphics[]{Fig/div_10_10}}
	\hfill\null
	
	\null\hfill
	\subfloat[][$ v_{fn} = 5$]{
	\includegraphics[]{Fig/div_10_50}}
	\hfill
	\subfloat[][$ v_{fn} = 10$]{
	\includegraphics[]{Fig/div_10_100}}
	\hfill\null
	\caption{Divergencia de Kullback-Leibler $ d_{\mathrm{KL}}\left( v_{fn}\mid \hat{v}_{fn}\right) $ como funci'on de $ \hat{v}_{fn} $.}
	\label{fig:kldiv}
\end{figure}
Las divergencias de Kullback-Leibler y de Euclides comparten la propiedad de que son convexas como funciones $\hat{v}_{fn})$. Esta caracter'istica se emplea en las demostraciones de convergencia de los algortimos de optimizaci'on num'erica del ap'endice \ref{ap:algorit}.

\subsection{Divergencia Itakura-Saito}

La divergencia Itakura-Saito se obtuvo por primera vez por \citet{Itakura68} al maximizar la verosimilitud del espectro de audio usando un modelo autoregresivo y fue presentada como una forma de medir la bondad de ajuste entre dos espectros de audio. Esta divergencia ha sido usada recientemente para el an'alisis de espectogramas de audio usando FMN; este trabajo ha sido desarrollado por \citet{Fevotte09}. 
 
\begin{align*}
	d_{\mathrm{IS}}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad = \quad \frac{v_{fn}}{\hat{v}_{fn}}%
	-\log \frac{v_{fn}}{\hat{v}_{fn}}-1
\end{align*}

\begin{figure}[ht*]
	\centering
	\null\hfill
	\subfloat[][$ v_{fn} = 0.5 $]{
	\includegraphics[]{Fig/div_0_5}}
	\hfill
	\subfloat[][$ v_{fn} = 1$]{
	\includegraphics[]{Fig/div_0_10}}
	\hfill\null
	
	\null\hfill
	\subfloat[][$ v_{fn} = 5$]{
	\includegraphics[]{Fig/div_0_50}}
	\hfill
	\subfloat[][$ v_{fn} = 10$]{
	\includegraphics[]{Fig/div_0_100}}
	\hfill\null
	\caption{Divergencia de Itakura-Saito $ d_{\mathrm{IS}}\left( v_{fn}\mid \hat{v}_{fn}\right) $ como funci'on de $ \hat{v}_{fn} $.}
	\label{fig:isdiv}
\end{figure}

Una propiedad interesante de la divergencia Itakura-Saito es que es invariante ante cambios de escala, esto es:
\begin{align*}
	d_{\mathrm{IS}}\left( \lambda v_{fn}\mid \lambda \hat{v}_{fn}\right) \quad = \quad d_{\mathrm{IS}}\left( v_{fn}\mid \hat{v}_{fn}\right) \qquad \forall \lambda > 0
\end{align*}
Esta propiedad significa que se considera con igual importancia relativa los coeficientes chicos y grandes de $V$ en la funci'on de costo, en el sentido de que un mal ajuste para un coeficiente peque'no estar'a igualmente penalizado que un mal ajuste para un coeficiente grande. Esto es diferente a las divergencias euclideana y Kullback-Leibler que asignan una mayor p'erdida a las divergencias en los coeficientes grandes.

Por otro lado, a diferencia de las otras funciones presentadas anteriormente en este cap'itulo, $d_{\mathrm{IS}}\left( v_{fn}\mid \hat{v}_{fn}\right)$ no es una funci'on convexa como funci'on de $\hat{v}_{fn}$.

\section{$\beta $-Divergencia}

La familia de $\beta $-divergencias es una familia parametrizadas por un par'ametro $\beta$ que contiene a las divergencias Euclidiana, Kullback-Leibler e Itakura-Saito como casos especiales ($\beta = 2,1,0$ respectivamente). La $\beta $-divergencia fue presentada por primera vez por \citet{Basu98}. Una discusi'on detallada sobre las propiedades de esta familia se puede encontrar en \citet{Cichocki10}.

\begin{align*}
	d_{\beta}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad =& \quad \frac{1}{\beta (\beta -1)}v_{fn}^{\beta }+(\beta -1)\hat{v}_{fn}^{\beta }-\beta
	v_{fn}\hat{v}_{fn}^{\beta -1} \\
	=& \quad \frac{v_{fn}^{\beta }-v_{fn}%
	\hat{v}_{fn}^{\beta -1}}{\beta -1}-\frac{v_{fn}^{\beta }}{\beta }+\frac{\hat{%
	v}_{fn}^{\beta }}{\beta }
\end{align*}

Se puede mostrar f'acilmente que:
\begin{align*}
	d_{\mathrm{EUC}}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad = & \quad \left. d_{\beta}\left( v_{fn}\mid \hat{v}_{fn}\right)\right\vert _{\beta =2} \\
	d_{\mathrm{KL}}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad = & \quad \lim_{\beta \rightarrow 1}d_{\beta}\left( v_{fn}\mid \hat{v}_{fn}\right) \\
	d_{\mathrm{IS}}\left( v_{fn}\mid \hat{v}_{fn}\right) \quad = & \quad \lim_{\beta \rightarrow 0}d_{\beta}\left( v_{fn}\mid \hat{v}_{fn}\right)
\end{align*}
usando la identidad $\lim_{\beta \rightarrow 0} \frac{x^\beta - y^\beta}{\beta}= \log \frac{x}{y}$ para calcular los l'imites.

Existen otras familias de divergencias diferentes a las que se presentan en este cap'itulo; por ejemplo, las divergencias de Bregman (que son una generalizaci'on de la familia de $\beta$-divergencias) y las divergencias de Csisz'ar. Estas familias tambi'en han sido usadas para FMN por \citet{Dhillon05} y \citet{Cichocki06b} respectivamente.

\begin{figure}[]
	\centering
	\null\hfill
	\subfloat[][$ \beta < 0 (\beta=-1) $]{
	\includegraphics[]{Fig/div_-10_10}}
	\hfill
	\subfloat[][$ 0 \leq \beta < 1 (\beta=0) $]{
	\includegraphics[]{Fig/div_0_10}}
	\hfill\null

	\null\hfill
	\subfloat[][$ \beta=1 $]{
	\includegraphics[]{Fig/div_10_10}}
	\hfill
	\subfloat[][$ 1 < \beta \leq 2 (\beta=1.5) $]{
	\includegraphics[scale = 0.85]{Fig/div_15_10}}
	\hfill\null

	\null\hfill
	\subfloat[][$ \beta=2 $]{
	\includegraphics[]{Fig/div_20_10}}
	\hfill
	\subfloat[][$ \beta > 2 (\beta=10) $]{
	\includegraphics[]{Fig/div_100_10}}
	\hfill\null
	\caption{$ \beta $-divergencia $ d_{\beta}\left( v_{fn}\mid \hat{v}_{fn}\right)$ como funci'on de $ \hat{v}_{fn} $ (con $ v_{fn} = 1 $) para diferentes valores de $ \beta$.}
\end{figure}
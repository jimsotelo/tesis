\chapter{Factorizaci'on de Matrices No Negativas}

\epigraph{``El todo es mayor a la suma de las partes''}{Arist'oteles}

La factorizaci'on de matrices no negativas fue popularizada por \citet{Lee99} con su art'iculo en la Revista Nature en 1999 al resaltar que los resultados de este algoritmo permiten un `aprendizaje basado en partes'. Esto es completamente diferente al `aprendizaje hol'istico' \,de otros algoritmos como el ACP. Este resultado es importante, debido a la intuici'on filos'ofica de que un objeto o concepto se puede representar como la suma de sus partes. La suma es una, entre muchas, formas de combinar las partes.

\section{Combinacion Lineal Aditiva}
Igual que en el caso del ACP, la pregunta que se intenta solucionar en la FMN es ?`C'omo modelar la estructura subyacente de las im'agenes de forma simplificada? La factorizaci'on de matrices no negativas se caracteriza por aproximar cada imagen por una combinaci'on lineal aditiva de ``caracter'isticas''. 

Adem'as, debido a que los pesos con los que se combinan estas ``caracter'isticas'' son siempre positivos, se puede interpretar f'acilmente esta combinaci'on lineal. Una posible interpretaci'on de la combinaci'on lineal equivale a pensar que para construir cada cara se pegan estampas transparentes de las caracter'isticas hasta construir una aproximaci'on a la imagen inicial.

Si $v$ es una imagen y $W_1,...,W_K > 0$ son ``caracter'isticas'', entonces:
\begin{center}
	$v\approx \sum\limits_{k=1}^{K}{H^kW_k}$
\end{center}
donde $H^k > 0$ son los pesos con los que las ``caracter'isticas'' se suman. Dicho de otra forma, $H = (H^1,...,H^K)$ son los pesos de la combinaci'on lineal de $v$ en la base de ``caracter'isticas''.

\section{Planteamiento del Problema}
Dada una matriz $V$ de dimensi'on $F\times N$ con entradas no negativas, el problema de factorizaci'on de matrices no negativas consiste en encontrar una factorizaci'on:
\begin{center}
	$V\approx WH=\hat{V}$
\end{center}
donde $W$ y $H$ son matrices con entradas no negativas de dimensi'on \mbox{$F\times K$} y \mbox{$ K\times N $} respectivamente. Igual que antes, la idea es que $K$ sea peque'na de tal forma que $\hat{V}$ sea una matriz de bajo rango. Las restricciones de no negatividad que se imponen en este problema causan que se obtenga una representaci'on basada en partes porque solamenten se permiten operaciones aditivas no sustractivas.

\subsection{Problema de Optimizaci'on}

Este problema se  plantea como uno de optimizaci'on:
\begin{equation*}
	\begin{aligned}
		& \underset{W,H}{\text{minimizar}}
		& & D(V||WH)=D(V||\hat{V}) =\sum\limits_{f=1}^{F}\sum\limits_{n=1}^{N}%
		d\left( v_{fn}\mid \hat{v}_{fn}\right) \\
		& \text{sujeto a}
		& & W,H \geq 0
	\end{aligned}
\end{equation*}
en donde $d(x|y) $ es una funci'on escalar de discrepancia o costo que penaliza la diferencia entre $x$ y $y$.

La funci'on $ d(x|y) $ juega un papel muy importante en el problema de optimizaci'on. En el cap'itulo \ref{cap:diverg} se presentan la familia de $\beta$-divergencias que suelen ser usadas en la pr'actica en la factorizaci'on de matrices no negativas. Los resultados que se obtienen de la factorizaci'on dependen de la funci'on de costo usada por lo cual es importante usar una funci'on que adecuada para el problema determinado. Para el resto del cap'itulo, se asume que $d(x|y) $ es la divergencia Euclideana, que se presenta formalmente en la secci'on \ref{sec:Euclid}.

\section{Resultados}

A continuaci'on se presentan dos aplicaciones en donde ha sido usado este algorimo. Los m'etodos para solucionar num'ericamente este problema se detallan en el ap'endice \ref{ap:algorit}. Estos m'etodos fueron desarrollados por \citet{Lee01} y posteriormente han sido extendidos por diversos investigadores como \citet{Fevotte11}, \citet{Li12}, \citet{Dhillon05} entre otros.

\subsection{Caras}

\begin{figure}[ht*]
	\captionsetup[subfigure]{labelformat=empty}
	\centering
	\null\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/est_nmf_500}}\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/est_nmf_1000}}\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/est_nmf_1500}}\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/est_nmf_2000}}
	\hfill\null
	\caption{Reconstrucci'on de las im'agenes usando FMN con $k = 40$ }
	\label{fig:NMFRecon}
\end{figure}

En la figura \ref{fig:NMFRecon} es posible apreciar que la recomposici'on de las im'agenes es de una calidad ligeramente menor que usando componentes principales. Sin embargo, la recomposici'on lograda tiene una calidad razonablemente parecida a la del ACP.

\begin{figure}[ht*]
	\captionsetup[subfigure]{labelformat=empty}
	\centering
	\null\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_1}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_2}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_3}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_4}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_5}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_6}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_7}}
	\hfill\null
	\\
	\null\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_8}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_9}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_10}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_11}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_12}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_13}}\hfill
	\subfloat[][]{\includegraphics[height = 20 mm, width = 20 mm]{Fig/nmf_14}}
	\hfill\null
	\caption{Caracter'isticas aprendidas por FMN con $k = 40$.}
	\label{fig:NMFComp}
\end{figure}

Lo m'as importante en la figura \ref{fig:NMFComp} es que las caracter'isticas identificadas usando FMN usan s'olo un subconjunto de variables, a diferencia de la representaci'on hol'istica de componentes principales. En algunas ``caracter'isticas'' podemos observar partes de caras como bocas, ojos o narices, mientras que en la figura \ref{fig:ACPComp} cada caracter'istica parece una cara completa.

\subsection{Letras de Canciones}

A continuaci'on se presentan los resultados de aplicar la FMN a la base de datos de letras de canciones presentada en la secci'on \ref{base:canciones}. En las tablas \ref{tbl:nickcave} y \ref{tbl:2pac}, cada caracter'istica (columna) est'a representada por las 15 palabras m'as importantes. El 'ultimo rengl'on corresponde al peso con el cual se pondera cada columna. Cabe destacar que estas 8 columnas corresponden al 68\% y 83\% de las canciones respectivamente.

\begin{table*}[htb]
\caption{Caracter'isticas m'as importantes para la canci'on ``Do You Love Me?'' de Nick Cave and the Bad Seeds.}
\label{tbl:nickcave}
\begin{center}
%\scriptsize
\begin{tabular}{cccccccc}
the & you & love & not & i & me & she & god \\
in & your & buy & do & am & give & her & of  \\
and & can & liar & wanna & myself & tell & girl & blood  \\
of & if & tender & care & like & call & beauti & soul  \\
world & know & dear & bad & know & mmm & woman & death  \\
with & want & instrument & nobodi & need & show & \& & die  \\
they & make & mood & anyth & want & beg & queen & fear \\
from & when & treasur & want & feel & rescu & sex & pain \\
as & see & emot & worri & caus & teas & sexi & hell \\
by & yourself & untru & ai & and & squeez & cloth & power \\
at & need & surrend & treat & out & everytim & herself & within \\
out & with & deeper & but & sorri & knee & doll & shall \\
to & feel & sparkl & know & see & strife & shes & earth \\
into & that & sweetest & money & in & contempl & pink & blind \\
sky & how & diamond & hurt & swear & guarante & gypsi & human \\\hline\hline
0.15 &0.10 &0.09 &0.08 &0.08 &0.08 &0.06 &0.04 
\end{tabular}
\end{center}
\end{table*}

\begin{table*}[htb]
\caption{Caracter'isticas m'as importantes para la canci'on ``California Love'' de 2pac.}
\label{tbl:2pac}
\begin{center}
\begin{tabular}{cccccccc}
get & the & shake & it & you & we & come & yeah \\
nigga & in & motion & is & your & our & babi & five \\
the & and & bump & take & can & us & magic & four \\
ya & of & groov & doe & if & togeth & til & woo  \\
shit & world & booti & make & know & both & lovin & summertim \\
like & with & shakin & easi & want & higher & im & girlfriend \\
fuck & they & thigh & matter & make & ourselv & shi & wow \\
em & from & oon & real & when & each & bodi & lala \\
got & as & shiver & game & see & divid & sweat & grip \\
hit & by & panic & possibl & yourself & nation & cant & pine \\
bitch & at & dick & play & need & unit & your & engin \\
up & out & claw & chanc & with & other & birthday & feather \\
off & to & opportun & give & feel & noel & wont & clap \\
yall & into & collid & harder & that & standard & there & mornin \\
ass & sky & ness & quit & how & rule & bella & gotta \\\hline\hline
0.49 &0.09 &0.08 &0.07 &0.03 &0.03 &0.02 &0.02
\end{tabular}
\end{center}
\end{table*}


\begin{landscape}
\thispagestyle{empty}
\begin{table*}[htb]
\caption{Canciones m'as representativas para ciertas ``caracter'isticas'' y palabras que aparecen m'as frecuentemente.}
\centering
\begin{tabular}{ll}
\hline\hline
\multicolumn{2}{c}{($k =  2$)  get nigga the ya shit like fuck em got hit bitch up off yall ass they that cmon money and} \\\hline
UGK (Underground Kingz) - Murder & i the to nigga my a you got murder and it is am from we so with they yo cuz \\
Big Punisher - Nigga Shit & shit that nigga the i and my what to out am in on for love me with gettin you do \\
 E-40 - Turf Drop [Clean] & gasolin the my i hey to a it on you some fuck spit of what one ride nigga sick gold \\
Cam'Ron - Sports Drugs \& Entertainment & a the you i got yo stop shot is caus or street jump short wick either to on but in\\
 Foxy Brown - Chyna Whyte & the nigga and you shit i not yall to a on with bitch no fuck uh it money white huh\\
\hline\hline
\multicolumn{2}{c}{($k = 8$) god of blood soul death die fear pain hell power within shall earth blind human bleed scream evil holi peac}\\\hline
  Demolition Hammer - Epidemic Of Violence & of pain death reign violenc and a kill rage vicious the to in down blue dead cold\\% blood hate lead\\
  Disgorge - Parallels Of Infinite Torture & of the tortur by their within upon flow throne infinit are no they see life eye befor\\% blood holi bless\\
  Tacere - Beyond Silence & silenc beyond a dark beauti i the you to and me it not in my is of your that do\\
  Cannibal Corpse - Perverse Suffering & to my pain of i me for agoni in by and from way etern lust tortur crave the not be\\
  Showbread - Sampsa Meets Kafka & to of no one die death loneli starv i the you and a me it not in my is your\\
\hline\hline
\multicolumn{2}{c}{($k = 26$) she her girl beauti woman \& queen sex sexi cloth herself doll shes pink gypsi bodi midnight callin dress hair} \\\hline
 Headhunter - Sex \& Drugs \& Rock'N Roll &  \& sex drug rock roll n is good veri inde and not my are all need dead bodi brain i \\
 Holy Barbarians - She & she of kind girl my is the a littl woman like world and gone destroy tiger me on an\\
 X - Devil Doll & devil doll her she and a the in is of eye bone \& shoe rag batter you to on no \\
 Kittie - Paperdoll & her she you i now soul pain to is down want eat fit size and not in all dead bodi \\
 Ottawan - D.I.S.C.O. & is she oh disco i o s d c super incred a crazi such desir sexi complic special candi\\
\hline\hline
\multicolumn{2}{c}{($k = 13$) je et les le pas dan pour des cest qui de tout mon moi au comm ne sur jai} \\\hline
 Veronique Sanson - Feminin & cest comm le car de bien se les mai a fait devant heur du et une quon quelqu etre\\
 Nevrotic Explosion - Heritage & quon faut mieux pour nous qui nos ceux de la un plus tous honor parent ami oui\\
 Kells - Sans teint & de la se le san des est loin peur reve pour sa sang corp lumier larm\\
 Stille Volk - Corps Magicien & de les ell dan la se le du pass est sa par mond leur corp vivr lair voyag feu\\
 Florent Pagny - Tue-Moi & si plus que un tu mon mes jour souvenir parc\\\hline\hline
\end{tabular}
\label{tbl:mxm_rep}
\end{table*}
\end{landscape}
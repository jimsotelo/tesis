\chapter{Introducci'on}
\pagenumbering{arabic}

\epigraph{``Ense'nar no es una funci'on vital porque no tiene el fin en s'i misma; la funci'on vital es aprender.''}{Arist'oteles}

Llegar a una conclusi'on sobre lo que significa ``aprender'' sin lugar a dudas entusiasmar'ia a tu fil'osofo de confianza. El concepto de aprendizaje comprende procesos tan diferentes que es dif'icil de definir con precisi'on. Algunas posibles definiciones de \emph{aprender} son: ``Adquirir conocimiento de algo por medio de estudio o de experiencia'', ``Incorporar algo en la memoria'',``Modificar la tendencia de comportamiento basado en experiencia''.

En este trabajo se presenta un nuevo algoritmo para seleccionar la funci'on de costo en el m'etodo de factorizaci'on de matrices no negativas. La factorizaci'on de matrices no negativas (FMN) fue popularizada por \citet{Lee99} en la comunidad de aprendizaje de m'aquina por sus propiedades para aprender estructuras basadas en partes. La FMN forma parte del conjunto de t'ecnicas conocido como \emph{Aprendizaje de m'aquina}.

\section{Aprendizaje de M'aquina}

A lo largo del tiempo, se ha buscado conseguir que las m'aquinas aprendan. Como \citet{Russell09} describen, durante los primeros a'nos de la inteligencia artificial (1952 - 1969) se lleg'o a pensar que en pocos a'nos las m'aquinas ser'ian capaces de superar a los seres humanos en algunas tareas particularmente complejas. Sin embargo, este optimismo llego a su fin debido a las limitaciones computacionales de ese tiempo. Incluso se abandon'o la esperanza de que una computadora ser'ia capaz de traducir un idioma en tiempo real, o que se crear'ia una inteligencia artificial capaz de mantener una conversaci'on con una persona.

A pesar del atraso por las limitaciones en la capacidad de c'omputo de la 'epoca, esos a'nos fueron muy fruct'iferos en cuanto a la cantidad de ideas sobre el aprendizaje que se generaron. En particular, se adaptaron teor'ias sobre el aprendizaje generadas en otros campos del conocimiento como la psicolog'ia. El origen de algunas de las t'ecnicas dise'nadas para que las m'aquinas puedan aprender, est'a en los esfuerzos de psic'ologos para crear modelos que puedan representar el aprendizaje humano y animal.

Por otra parte, desde hace algunos a'nos, la cantidad de datos disponibles para efectos de an'alisis ha crecido de forma impresionante. La comunidad cient'ifica ha respondido con algoritmos que permiten extraer informaci'on valiosa de los datos. El campo de procesamiento de se'nales fue de los primeros en encontrarse al problema de extraer informaci'on de una base de datos, y de este campo surgieron algunos de los algoritmos m'as populares.

Debido a la creciente importancia de los problemas relacionados con la extracci'on de informaci'on, el estudio de los diferentes algoritmos se ha consolidado en un campo emergente conocido como ``Aprendizaje de M'aquina''. De acuerdo con \citet{Nilsson96}, una m'aquina aprende siempre que cambia su estructura, programas, o datos (basada en insumos o en respuesta a informaci'on externa) de tal manera que su desempe'no futuro tiende a mejorar. Nilsson comenta que el aprendizaje de m'aquina normalmente se refiere a cambios en sistemas que realizan actividades asociadas al campo de Inteligencia Artificial. Esas labores involucran reconocer patrones, elaborar diagn'osticos, controlar robots, predecir variables, entre otras.

Los avances y las ideas en el aprendizaje de m'aquina han provenido de diferentes disciplinas. Cada una con sus propios problemas, m'etodos y notaciones. Algunas de las que han contribuido son:
\begin{itemize}
	\item Estad'istica
	\item Computaci'on
	\item Inteligencia Artificial
	\item Investigaci'on de Operaciones
	\item Optimizaci'on Num'erica
	\item Neurociencia
	\item Psicolog'ia
\end{itemize}
El campo del aprendizaje de m'aquina se ocupa de diversos temas y es muy heterog'eneno en lo que respecta a las perspectivas con que se ha aproximado a las soluciones de los problemas. Una complicaci'on que vale la pena tener en cuenta es que la notaci'on suele ser diferente seg'un la perspectiva desde la cual se plantee un problema.

\section{Aprendizaje Estad'istico}

La estad'istica es constantemente desafiada por diferentes ramas de la ciencia, as'i como por la industria. Tiempo atr'as, los problemas sol'ian provenir de experimentos relativamente peque'nos de la agricultura y de la industria. Dado el desarrollo de los sistemas de informaci'on y del poder de c'omputo disponible, los problemas se han hecho cada vez m'as grandes y complejos. Hoy en d'ia se generan vol'umenes de datos impresionantes. Usando las t'ecnicas estad'isticas existendes se puede \emph{aprender de los datos}; esto es, extraer patrones y tendencias, y entender lo que los datos ``dicen''. El \emph{Aprendizaje Estad'istico} se refiere a un amplio conjunto de herramientas que sirven para entender los datos.

Tradicionalmente, una de las labores que los estad'isticos han enfrentado es resolver problemas que requieren extraer res'umenes de informaci'on contenida en datos. Para lograr hacer inferencia de los datos se suelen plantear modelos estad'isticos que representen el estado subyacente del mundo que gener'o ciertos datos y despu'es extraer par'ametros clave. La caracter'istica que hace diferente al aprendizaje estad'istico del aprendizaje de m'aquina es la b'usqueda de un modelo de probabilidad subyacente dentro de una conjunto de datos. Con este modelo se busca lograr representar la variabilidad que existe en los datos.

A grandes rasgos, todas las t'ecnicas de aprendizaje estad'istico involucran la construcci'on de un modelo estad'istico para predecir un resultado, basado en uno o m'as insumos o para descubrir patrones en los datos. Los problemas que se pueden abordar usando este enfoque se caracterizan por describir fen'omenos que se manifiestan a trav'es de datos que presentan variabilidad e incluyen diversos campos como f'isica, econom'ia, medicina, finanzas y muchos m'as.

\subsection{Historia del Aprendizaje Estad'istico}
Aunque el t'ermino ``aprendizaje estad'istico'' es relativamente nuevo, muchas de las t'ecnicas que forman parte de este campo se desarrollaron mucho tiempo atr'as. Al inicio del siglo $XIX$, se desarroll'o el m'etodo de m'inimos cuadrados que es el primer m'etodo de ajuste de curvas y que se encuentra en el origen de la regresi'on lineal. Para el problema de clasificaci'on en grupos, Fischer propuso el an'alisis de discriminante lineal m'as de un siglo despu'es en 1936 y en 1940 se propuso la regresi'on logistica. Para 1980 el poder de c'omputo hab'ia mejorado lo suficiente que se pudieron explorar m'etodos no lineales. As'i, en 1984 se ten'ia por primera vez una implementaci'on de los 'arboles de clasificaci'on y regresi'on de la mano de \citet{Breiman84}. En los 'ultimos a'nos se han generado distintos m'etodos que complementan y generalizan los m'etodos existentes. Actualmente, las t'ecnicas de aprendizaje estad'istico se encuentran en expansi'on con nuevas ideas que prometen mejorar los l'imites actuales en la predicci'on y descubrimiento de patrones en bases de datos cada d'ia m'as desafiantes. La mejor introducci'on a este campo se encuentra en \citet{James13} que complementa el trabajo anterior de \citet{Hastie09} que se dirige a un p'ublico m'as t'ecnico.

\section{Aprendizaje Supervisado}
El problema t'ipico de aprendizaje supervisado consiste en predecir el valor de una variable respuesta $ Y $ dado un conjunto de variables de entrada $X^T=(X_1, \ldots, X_p)$. Las predicciones estan basadas en un conjunto de entrenamiento y se suele referir a este tipo de aprendizaje como  ``Aprender con un maestro''. La met'afora detras de esta expresi'on es que el modelo o ``estudiante''\, presenta una respuesta $\hat y$ para cada $x = (x_1, \ldots, x_p)$ del conjunto de entrenamiento, y el ``maestro'' provee la respuesta corecta y/o un error asociado a la respuesta del estudiante. Esto suele ser caracterizado por una funci'on de p'erdida $L$ que penaliza los errores cometidos por el ``estudiante''.

\section{Aprendizaje No supervisado}
El aprendizaje no supervisado consiste de un conjunto de herramientas enfocadas a la situaci'on en la que solamente se cuenta con un conjunto de $n$ observaciones de las variables $X_1, X_2, \ldots, X_p$. En este contexto, no se puede hablar de predicci'on dado que no hay variables respuestas que predecir. En lugar de eso, el objetivo es descubrir patrones interesantes o estructura en los datos con los que se cuenta. En general, es un campo menos desarrollado que el aprendizaje supervisado, aunque no por eso menos importante.

El aprendizaje supervisado es un campo con un objetivo claro, predecir de la mejor forma variables respuesta a partir de observaciones de una colecci'on de variables. M'as aun, existe una forma clara de cuantificar la calidad de los resultados obtenidos: Evaluando la predicci'on en un conjunto de prueba que no fue usado para entrenar el modelo.

El aprendizaje no supervisado es mucho m'as desafiante. En este campo existe mucha m'as subjetividad y no hay una meta clara para los an'alisis. Como consecuencia, puede llegar a ser muy dif'icil evaluar la calidad de los resultados obtenidos. Dicho de otra forma, no hay forma de saber si el trabajo que se realiza est'a bien debido a que la respuesta correcta es desconocida, si es que esta existe.

Algunas de las preguntas que se pueden explorar usando t'ecnicas de aprendizaje no supervisado son: ?`Qu'e tipo de visualizaciones podemos usar para extraer informaci'on de los datos? ?`Existen subgrupos dentro de las variables o dentro de las observaciones de los datos? ?`Se pueden encontrar observaciones dentro de la base que puedan caracterizar subgrupos en los datos?.

Tambi'en vale la pena destacar que es com'un dentro de un an'alisis estad'istico usar las t'ecnicas de aprendizaje no supervisado disponibles como parte del proceso de exploraci'on de datos despu'es hacer uso de los resultados de los an'alisis realizados para intentar predecir m'as eficazmente alguna variable de inter'es; un ejemplo muy com'un de esto es cuando se usan los componentes principales en una regresi'on.

\section{Bases de Datos}
A continuaci'on se presenta una descripci'on breve de las bases de datos utilizadas en este trabajo. Con estas bases de datos se busca que la explicaci'on de los algoritmos presentados sea clara y suficiente para reconocer las diferencias entre las diferentes alternativas.

\begin{figure}[ht*]
	\captionsetup[subfigure]{labelformat=empty}
	\centering
	\null\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/cara_500}}\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/cara_1000}}\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/cara_1500}}\hfill
	\subfloat[][]{\includegraphics[height = 30 mm, width = 30 mm]{Fig/cara_2000}}\hfill\null
	\caption{Ejemplos de im'agenes de caras.}
	\label{fig:EjemploCaras}
\end{figure}

\subsection{Caras}
\label{base:caras}
Esta base consiste de 2,429 im'agenes de caras de baja resoluci'on usadas por \citet{Lee99}. La resoluci'on de cada imagen es de $19 \times 19$ pixeles en escala de grises. En la figura \ref{fig:EjemploCaras} se muestran algunos ejemplos del tipo de im'agenes de las que consiste la base de datos.

\subsection{Letras de Canciones}
\label{base:canciones}
Esta base consiste de conteos de palabras en la letra de 237,701 canciones. En total, existen 498,134 palabras 'unicas y 55,163,335 ocurrencias de estas palabras. Por simplicidad, s'olo se analizaron las 5,000 palabras que ocurren m'as frecuentemente en las canciones. Estas 5000 palabras aparecen 50,607,582 veces, con lo cual representan aproximadamente el 92 \% de las palabras en total. Esta base fue construida por \citet{Bertin11} y es reconocida como la base de letras de canciones m'as grande y limpia disponible para investigaci'on.

\subsection{Piano}
\label{base:piano}
Los datos provienen de la grabaci'on de un piano Yamaha DisKlavier MX100A ubicado al tocar 4 notas; primero todas al mismo tiempo, y despu'es por pares en todas las combinaciones posibles. La grabaci'on dur'o 15.6 segundos y se construye el espectrograma de la grabaci'on usando la transformada de Fourier de tiempo reducido, que consiste de una matriz que contiene la intensidad de distintas se'nales en el tiempo. Se consideran 513 diferentes frecuencias en 674 cortes en el tiempo. En la figura \ref{fig:Piano} se presentan las notas, la se'nal en el tiempo y su espectrograma. Para m'as informaci'on sobre estas y otras transformaciones comunes asociadas a datos provenientes de audio se puede consultar \citet{Wang11}.

\begin{figure}[]
	\centering
	\subfloat[][ Notas musicales ]{
	\includegraphics[]{Im/pentagrama}}

	\subfloat[][ Se'nal]{
	\includegraphics[scale = .8]{Im/senal}}
	
	\subfloat[][ Espectrograma]{
	\includegraphics[scale = .8]{Im/espectrograma}}

	\caption{Distintas representaciones de los datos de sonido.}
	\label{fig:Piano}
\end{figure}

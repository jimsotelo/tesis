import sys

def main():
	data_file = open (sys.argv[1])

	for line in data_file:

		if line[0] == '#':
			pass

		elif line[0] == '%':
			line = line[1:]
			L = line.split(',')

		else:
			E = line.strip().split(',')
			D = {k:0 for k in range(1,5001)}

			for pair in E[2:]:
				k,v = pair.split(':')
				k = int(k)
				v = int(v)
				D[k] = v

			S = [str(D[k]) for k in range(1,5001)]

			print E[1]+ ',' + ','.join(S)

if __name__ == '__main__':
	main()